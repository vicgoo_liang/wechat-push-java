package cn.cvzhanshi.wechatpush.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zkb
 * @since 2022-09-19
 */
@RestController
@RequestMapping("/user")
public class UserController {

}
