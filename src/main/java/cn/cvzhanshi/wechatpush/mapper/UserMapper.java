package cn.cvzhanshi.wechatpush.mapper;

import cn.cvzhanshi.wechatpush.model.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zkb
 * @since 2022-09-19
 */
public interface UserMapper extends BaseMapper<User> {

}
