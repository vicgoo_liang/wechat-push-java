package cn.cvzhanshi.wechatpush.service;

import cn.cvzhanshi.wechatpush.model.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zkb
 * @since 2022-09-19
 */
public interface UserService extends IService<User> {

}
