package cn.cvzhanshi.wechatpush.service.impl;

import cn.cvzhanshi.wechatpush.model.User;
import cn.cvzhanshi.wechatpush.mapper.UserMapper;
import cn.cvzhanshi.wechatpush.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zkb
 * @since 2022-09-19
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
