package cn.cvzhanshi.wechatpush.config;


import cn.cvzhanshi.wechatpush.entity.Weather;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;

import java.util.Map;

/**
 * @author cVzhanshi
 * @create 2022-08-04 21:09
 */
public class Pusher {

    public static void main(String[] args) {
        push("oawV76PFW-FDINvZb1J-RTC44EDQ");
    }
    private static String appId = "wx969d00a289f6bc56";
    private static String secret = "4a335ce53e8900af55025b1347375bdf";



    public static void push(String userId){
        //1，配置
        WxMpInMemoryConfigStorage wxStorage = new WxMpInMemoryConfigStorage();
        wxStorage.setAppId(appId);
        wxStorage.setSecret(secret);
        WxMpService wxMpService = new WxMpServiceImpl();
        wxMpService.setWxMpConfigStorage(wxStorage);
        //2,推送消息
        WxMpTemplateMessage templateMessage = WxMpTemplateMessage.builder()
                //.toUser("oawV76Cp48P4z_zTkfFPeIg2tMY4")
                .toUser(userId)
                .templateId("lLnVnPa2z918iHLsSA3_eXlpU7N04dYyYXFZUxps1ag")
                .build();
        //3,如果是正式版发送模版消息，这里需要配置你的信息
        Weather weather = WeatherUtils.getWeather();
        Map<String, String> map = CaiHongPiUtils.getEnsentence();
        templateMessage.addData(new WxMpTemplateData("saylove",CaiHongPiUtils.saylove()+" \r\n","#FF69B4"));
        templateMessage.addData(new WxMpTemplateData("riqi",weather.getDate() + "  "+"\r\n"+ weather.getWeek(),"#00BFFF"));
        templateMessage.addData(new WxMpTemplateData("tianqi",weather.getText_now()+"\r\n","#00FFFF"));
        templateMessage.addData(new WxMpTemplateData("low",weather.getLow() + " 度"+"\r\n","#173177"));
        templateMessage.addData(new WxMpTemplateData("temp",weather.getTemp() + " 度"+"\r\n","#EE212D"));
        templateMessage.addData(new WxMpTemplateData("high",weather.getHigh()+ " 度"+"\r\n","#FF6347" ));
        templateMessage.addData(new WxMpTemplateData("windclass",weather.getWind_class()+ " "+"\r\n","#42B857" ));
        templateMessage.addData(new WxMpTemplateData("winddir",weather.getWind_dir()+ ""+"\r\n","#B95EA3" ));
        templateMessage.addData(new WxMpTemplateData("caihongpi",CaiHongPiUtils.getCaiHongPi()+" \r\n","#FF69B4"));
        templateMessage.addData(new WxMpTemplateData("lianai",JiNianRiUtils.getLianAi()+" 天"+"\r\n","#FF1493"));


        templateMessage.addData(new WxMpTemplateData("en",map.get("en") +""+"\r\n","#C71585"));
        templateMessage.addData(new WxMpTemplateData("zh",map.get("zh") +""+"\r\n","#C71585"));
        String beizhu = "早安我的宝贝～洪雪"+"\r\n";

        //beizhu = "今天是恋爱" + JiNianRiUtils.getLianAi() + "天！";

//        templateMessage.addData(new WxMpTemplateData("shengri",JiNianRiUtils.getBirthday_Jo()+"","#FFA500"));
//        if(JiNianRiUtils.getBirthday_Jo()  == 0){
//            beizhu = "今天是老公的生日，生日快乐呀！";
//
//        }

        templateMessage.addData(new WxMpTemplateData("shengri",JiNianRiUtils.getBirthday_Hui()+" 天"+"\r\n ","#FFA500"));
        if(JiNianRiUtils.getBirthday_Hui()  == 0){
            beizhu = "今天是洪雪宝贝生日，生日快乐呀！";

        }
        templateMessage.addData(new WxMpTemplateData("beizhu",beizhu+"\r\n","#FF0000"));

        try {
            System.out.println(templateMessage.toJson());
            System.out.println(wxMpService.getTemplateMsgService().sendTemplateMsg(templateMessage));
        } catch (Exception e) {
            System.out.println("推送失败：" + e.getMessage());
            e.printStackTrace();
        }
    }
}
