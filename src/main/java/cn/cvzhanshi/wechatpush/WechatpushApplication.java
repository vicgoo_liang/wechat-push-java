package cn.cvzhanshi.wechatpush;

import cn.cvzhanshi.wechatpush.config.Pusher;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@EnableScheduling
@MapperScan("cn.cvzhanshi.wechatpush.mapper")
public class WechatpushApplication {

    public static void main(String[] args) {
        SpringApplication.run(WechatpushApplication.class, args);
    }


    @Scheduled(cron = "0 30 7 * * ?")
    public void goodMorning(){
        Pusher.push("oawV76PFW-FDINvZb1J-RTC44EDQ");
        Pusher.push("oawV76Cp48P4z_zTkfFPeIg2tMY4");
    }

}
